###Тестовое задание на должность full-stack web developer
Реализовать каталог сотрудников, который может работать в offline. 
 
* При первоначальной загрузке, данные сохраняются в localstorage и последующее отключение не влияет на работу.
* Загрузка фотографий сотрудников осуществляется и в offline, фотографии сохраняются в localstorage и как только стали online, все изменения загружаются на сервер.

### There are Laravel 5 backend with REST API and Angular 6 frontend application.

## Requirements

* PHP 7.1
* MySQL 5.7

## Clone repository

* git clone --recurse-submodules -j8 https://bitbucket.org/Breedinf/si_test/src/master/ .
* git submodule update --recursive --remote

## Backend setup

* create mysql database
* edit backend/.env and setup variables DB_DATABASE, DB_USERNAME, DB_PASSWORD


* cd backend
* composer install
* php artisan migrate
* php artisan db:seed


## Start backend

* php artisan serve

## Frontend setup

* open new console window
* cd frontend
* npm install
* npm start

## Open http://localhost:4200 in any modern browser

## Use it...

## Check if offline mode is works

* stop backend (Ctrl+C in console)
* upload avatar for any employee
* start backend again (php artisan serve)
* wait several seconds: app have to alert that all photos are uploaded
